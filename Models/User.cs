namespace KlikovnikAPIV3.Models
{
    public partial class User
    {
        public int UserId {get; set;}
        public string FirstName {get; set;} = "";
        public string LastName {get; set;} = "";
        public string Email {get; set;} = "";
        public string Phone {get; set;} = "";
        public string City {get; set;} = "";
    }
}