namespace KlikovnikAPIV3.Data
{
    public partial class PushUps
    {
        public string StartDateString {get; set;} = "";
        public int StartPushUpsCount {get; set;}
        public int MaxPushUpsByDay {get; set;}
        public int IncrementPushUpsByDay {get; set;}
        public List<PushUpsDay> PushUpsDay {get; set;} = new();
    }
}