namespace KlikovnikAPIV3.Models.DTO
{
    public partial class UserForRegistrationDTO
    {
        public string Email {get; set;} = "";
        public string FirstName {get; set;} = "";
        public string LastName {get; set;} = "";
        public string Phone {get; set;} = "";
        public string City {get; set;} = "";
        public string Password {get; set;} = "";
        public string PasswordConfirm {get; set;} = "";
    }
}