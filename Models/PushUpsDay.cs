namespace KlikovnikAPIV3.Data
{
    public partial class PushUpsDay
    {
        public string PushUpDayDateString {get; set;} = "";
        public int ExtraPushUps {get; set;}
        public bool Complete {get; set;}
    }
}