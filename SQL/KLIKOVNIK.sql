USE KlikovnikDatabase1

CREATE SCHEMA KlikovnikSchema
GO

--DROP TABLE KlikovnikSchema.Auth
CREATE TABLE KlikovnikSchema.Auth
(
    Email NVARCHAR(50) PRIMARY KEY,
    PasswordHash VARBINARY(MAX),
    PasswordSalt VARBINARY(MAX)
)
GO

SELECT * FROM KlikovnikSchema.Auth

CREATE TABLE KlikovnikSchema.Users
(
    UserId INT IDENTITY(1,1) PRIMARY KEY,
    Email VARCHAR(255),
    FirstName NVARCHAR(50),
    LastName NVARCHAR(50),
    Phone NVARCHAR(13),
    City VARCHAR(255)
)
GO

--DROP TABLE KlikovnikSchema.PushUpsInitialData
CREATE TABLE KlikovnikSchema.PushUpsInitialData
(
    PushUpsId INT IDENTITY(1,1) PRIMARY KEY,
    UserId INT,
    StartDateString VARCHAR(255),
    StartPushUpsCount INT,
    MaxPushUpsByDay INT,
    IncrementPushUpsByDay INT,
    FOREIGN KEY (UserId) REFERENCES KlikovnikSchema.Users
)
GO

--DROP TABLE KlikovnikSchema.PushUpsDay
CREATE TABLE KlikovnikSchema.PushUpsDay
(
    PushUpDayId INT IDENTITY(1,1) PRIMARY KEY,
    UserId INT,
    PushUpDayDateString VARCHAR(255),
    ExtraPushUps INT,
    Complete BIT,
    FOREIGN KEY(UserId) REFERENCES KlikovnikSchema.Users
)
GO
