USE KlikovnikDatabase1
GO

CREATE OR ALTER PROCEDURE KlikovnikSchema.spPushUpsInitialData_Insert
	@StartDayString NVARCHAR(255),
    @StartPushUpsCount INT,
    @MaxPushUpsByDay INT,
    @IncrementPushUpsByDay INT,
    @UserId INT

AS
BEGIN
    INSERT INTO KlikovnikSchema.PushUpsInitialData(
        [UserId],
        [StartDateString],
        [StartPushUpsCount],
        [MaxPushUpsByDay],
        [IncrementPushUpsByDay]
    ) VALUES (
        @UserId,
        @StartDayString,
        @StartPushUpsCount,
        @MaxPushUpsByDay,
        @IncrementPushUpsByDay
    )
END
