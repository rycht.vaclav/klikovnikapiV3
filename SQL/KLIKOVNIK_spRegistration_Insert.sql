USE KlikovnikDatabase1
GO

CREATE OR ALTER PROCEDURE KlikovnikSchema.spRegistration_Insert
    @Email NVARCHAR(50),
    @PasswordHash VARBINARY(MAX),
    @PasswordSalt VARBINARY(MAX)
AS
BEGIN
    IF NOT EXISTS(SELECT * FROM KlikovnikSchema.Auth WHERE Email = @Email)
        BEGIN
        INSERT INTO KlikovnikSchema.Auth(
                [Email],
                [PasswordHash],
                [PasswordSalt]
            ) VALUES (
                @Email,
                @PasswordHash,
                @PasswordSalt
            )
        END
    ELSE
        BEGIN
            UPDATE KlikovnikSchema.Auth
                SET PasswordHash = @PasswordHash,
                    PasswordSalt = @PasswordSalt
                WHERE Email = @Email
        END 
END

