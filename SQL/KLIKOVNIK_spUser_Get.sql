USE KlikovnikDatabase1
GO

CREATE OR ALTER PROCEDURE KlikovnikSchema.spUser_Get
    @UserId INT = NULL
AS
BEGIN

    SELECT [Users].[UserId],
        [Users].[Email],
        [Users].[FirstName],
        [Users].[LastName],
        [Users].[Phone],
        [Users].[City]
    FROM KlikovnikSchema.Users AS Users
        WHERE Users.UserId = ISNULL(@UserId, Users.UserId)
END