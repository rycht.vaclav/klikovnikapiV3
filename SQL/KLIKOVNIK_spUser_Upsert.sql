USE KlikovnikDatabase1
GO

CREATE OR ALTER PROCEDURE KlikovnikSchema.spUser_Upsert
	@FirstName NVARCHAR(50),
    @LastName NVARCHAR(50),
	@Email NVARCHAR(50),
    @Phone NVARCHAR(13),
    @City NVARCHAR(255),
    @UserId INT = NULL

AS
BEGIN

    IF NOT EXISTS (SELECT * FROM KlikovnikSchema.Users WHERE UserId = @UserId)
        BEGIN
        IF NOT EXISTS (SELECT * FROM KlikovnikSchema.Users WHERE Email = @Email)
            BEGIN
                --DECLARE @OutputUserId INT

                INSERT INTO KlikovnikSchema.Users(
                    [FirstName],
                    [LastName],
                    [Email],
                    [Phone],
                    [City]
                ) VALUES (
                    @FirstName,
                    @LastName,
                    @Email,
                    @Phone,
                    @City
                )

                --SET @OutputUserId = @@IDENTITY

                /*INSERT INTO KlikovnikSchema.PushUps (
                    UserId,
                    StartDateString,
                    StartPushUpsCount,
                    MaxPushUpsByDay,
                    IncrementPushUpsByDay
                ) VALUES (
                    @OutputUserId,
                    @StartDayString,
                    @StartPushUpsCount,
                    @MaxPushUpsByDay,
                    @IncrementPushUpsByDay
                )*/
            END
        END
    ELSE
        BEGIN
            UPDATE KlikovnikSchema.Users
                SET FirstName = @FirstName,
                LastName = @LastName,
                Email = @Email,
                Phone = @Phone,
                City = @City
                WHERE UserId = @UserId
        END
END
