USE KlikovnikDatabase1
GO 

CREATE OR ALTER PROCEDURE KlikovnikSchema.spUser_Delete
    @UserId INT
AS
BEGIN
    DELETE FROM KlikovnikSchema.Users
        WHERE UserId = @UserId 
    DELETE FROM KlikovnikSchema.PushUps
        WHERE UserId = @UserId
    DELETE FROM KlikovnikSchema.PushUpsDay
        WHERE UserId = @UserId
END