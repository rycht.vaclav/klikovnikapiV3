USE KlikovnikDatabase1
GO

CREATE OR ALTER PROCEDURE KlikovnikSchema.spLoginConfirmation_Get
    @Email NVARCHAR(50)
AS
BEGIN
    SELECT [Auth].[PasswordHash],
        [Auth].[PasswordSalt]
    FROM KlikovnikSchema.Auth AS Auth
        WHERE Auth.Email = @Email
END