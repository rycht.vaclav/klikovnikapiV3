using System.Data;
using System.Runtime.InteropServices;
using AutoMapper;
using Dapper;
using KlikovnikAPIV3.Data;
using KlikovnikAPIV3.Helpers;
using KlikovnikAPIV3.Models;
using KlikovnikAPIV3.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KlikovnikAPIV3.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AuthController: ControllerBase
    {
        private readonly DataContextDapper _dapper;
        private readonly AuthHelper _authHelper;
        private readonly SqlHelper _sqlHelper;
        private readonly IMapper _mapper;
        public AuthController(IConfiguration configuration)
        {
            _dapper = new(configuration);
            _authHelper = new(configuration);
            _sqlHelper = new(configuration);
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserForRegistrationDTO, User>();
            }));
        }

        [AllowAnonymous]
        [HttpPost("registration")]
        public IActionResult Registration(UserForRegistrationDTO userForRegistrationDTO)
        {
            if (userForRegistrationDTO.Password == userForRegistrationDTO.PasswordConfirm)
            {
                string? emailVerification = _authHelper.CheckEmailFormat(userForRegistrationDTO.Email);
                if (emailVerification != null)
                {
                    throw new Exception(emailVerification);
                }
                
                if (_sqlHelper.CheckIfEmailAlreadyExist(userForRegistrationDTO.Email))
                {
                    UserForLoginDTO userForSetPassword = new() {
                        Email = userForRegistrationDTO.Email,
                        Password = userForRegistrationDTO.Password
                    };

                    if (_authHelper.SetPassword(userForSetPassword))
                    {
                        User user = _mapper.Map<User>(userForRegistrationDTO);

                        string? phoneValidationAnswer = _authHelper.CheckNumberFormat(user.Phone);
                        if (phoneValidationAnswer != null)
                        {
                            throw new Exception(phoneValidationAnswer);
                        }

                        if (_sqlHelper.UpsertUser(user))
                        {
                            return Ok();
                        }

                        throw new Exception("Faild to add user to DB");
                    }
                    else
                    {
                        throw new Exception("Faild to add user to DB");
                    }
                }
                else
                {
                    throw new("User with email already exist.");
                }

            } else {
                //Look how to make your own error code
                throw new("Password are not the same.");
            }
            throw new("Can't registrate new user!");
        }
    }
}