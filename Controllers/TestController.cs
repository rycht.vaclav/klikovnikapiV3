using KlikovnikAPIV3.Data;
using Microsoft.AspNetCore.Mvc;

namespace KlikovnikAPIV3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        private readonly DataContextDapper _dapper;
        public TestController(IConfiguration configuration)
        {
            _dapper = new(configuration);
        }

        [HttpGet("Connection")]
        public DateTime TestConnection()
        {
            return _dapper.LoadDataSingle<DateTime>("SELECT GETDATE()");
        }

        [HttpGet]
        public string Test()
        {
            return "Your application is up and running";
        } 
    }
}