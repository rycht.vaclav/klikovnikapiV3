using System.Data;
using Dapper;
using KlikovnikAPIV3.Data;
using KlikovnikAPIV3.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KlikovnikAPIV3.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserController
    {
        private readonly DataContextDapper _dapper;
        public UserController(IConfiguration configuration)
        {
            _dapper = new(configuration);
        }

        [AllowAnonymous] // TODO: Dont forget to remove
        [HttpGet("GetUsers/{userId?}")]
        public IEnumerable<User> GetUsers(int? userId = 0)
        {
            string sql = "EXEC KlikovnikSchema.spUser_Get";
            string stringParameters = "";
            DynamicParameters sqlParameters = new();
        
            if(userId != 0)
            {
                stringParameters += "@UserId=@UserId";
                sqlParameters.Add("@UserId", userId, DbType.Int32);
            }

            if(stringParameters.Length > 0)
            {
                sql += stringParameters.Substring(1);
            }

            return _dapper.LoadDataWithParameters<User>(sql, sqlParameters);
        }
    }
}