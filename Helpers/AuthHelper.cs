using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Dapper;
using KlikovnikAPIV3.Data;
using KlikovnikAPIV3.Models;
using KlikovnikAPIV3.Models.DTO;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.IdentityModel.Tokens;

namespace KlikovnikAPIV3.Helpers
{
    public class AuthHelper
    {
        private readonly IConfiguration _config;
        private readonly SqlHelper _sql;
        public AuthHelper(IConfiguration configuration)
        {
            _config = configuration;
            _sql = new(configuration);
        }

        public byte[] GetPasswordHash(string password, byte[] passwordSalt)
        {
            string passwordSaltPlusString = _config.GetSection("AppSettings:PasswordKey").Value +
                Convert.ToBase64String(passwordSalt);

            return KeyDerivation.Pbkdf2(
                password: password,
                salt: Encoding.ASCII.GetBytes(passwordSaltPlusString),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8 
            );
        }

        public string CreateToken(int userId)
        {
            Claim[] claims = new Claim[] {
                    new Claim("userId", userId.ToString())
                };

            SymmetricSecurityKey tokenKey = new(
                    Encoding.UTF8.GetBytes(
                        _config.GetSection("Appsettings:TokenKey").Value
                    )
                );

            SigningCredentials credentials = new(
                    tokenKey, 
                    SecurityAlgorithms.HmacSha512Signature
                );

            SecurityTokenDescriptor descriptor = new()
                {
                    Subject = new ClaimsIdentity(claims),
                    SigningCredentials = credentials,
                    Expires = DateTime.Now.AddDays(1)
                };

            JwtSecurityTokenHandler tokenHandler = new();

            SecurityToken token = tokenHandler.CreateToken(descriptor);
        
            return tokenHandler.WriteToken(token);
        }

        public bool SetPassword(UserForLoginDTO userForSetPassword)
        {
            byte[] passwordSalt = new byte[128 / 8];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
                {
                    rng.GetNonZeroBytes(passwordSalt);
                }

            byte [] passwordHash = GetPasswordHash(userForSetPassword.Password, passwordSalt);

            return _sql.InsertAuth(userForSetPassword, passwordHash, passwordSalt);
        }


        public string? CheckEmailFormat(string email)
        {
            string pattern = @"^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
            Regex regex = new(pattern);

            if (!regex.IsMatch(email))
            {
                return Constants.WrongEmailFormat;
            }
            return null;
        }

        // Maybe should add null or empty check because of nonmandatory information about user?
        public string? CheckNumberFormat(string number)
        {
            if (number.Length == 13)
            {
                string dialCode = number[..4];
                if (Constants.DialCodes.Contains(dialCode))
                {
                    return null;
                }
                else 
                {
                    return Constants.NonExistingDial;
                }
            }
            return Constants.WrongNumberFormat;
        }
    }
}