using System.Data;
using Dapper;
using KlikovnikAPIV3.Data;
using KlikovnikAPIV3.Models;
using KlikovnikAPIV3.Models.DTO;

namespace KlikovnikAPIV3.Helpers
{
    public class SqlHelper
    {
        private readonly DataContextDapper _dapper;
        public SqlHelper(IConfiguration configuration)
        {
            _dapper = new(configuration);
        }

         public bool UpsertUser(User user)
        {
            DynamicParameters sqlParameters = new();
            sqlParameters.Add("@FirstNameParam", user.FirstName, DbType.String);
            sqlParameters.Add("@LastNameParam", user.LastName, DbType.String);
            sqlParameters.Add("@EmailParam", user.Email, DbType.String);
            sqlParameters.Add("@UserIdParam", user.UserId, DbType.Int32);
            sqlParameters.Add("@PhoneParam", user.Phone, DbType.String);
            sqlParameters.Add("@CityParam", user.City, DbType.String);


            string sql = @"EXEC KlikovnikSchema.spUser_Upsert
                @FirstName = @FirstNameParam, 
                @LastName = @LastNameParam, 
                @Email = @EmailParam, 
                @UserId = @UserIdParam,
                @Phone = @PhoneParam,
                @City = @CityParam";

            return _dapper.ExecuteSqlWithParameters(sql, sqlParameters);
        }

        public bool InsertPushUpsInitialData(PushUps pushUps)
        {
            DynamicParameters sqlParameters = new();
            sqlParameters.Add("@StartDayString", pushUps.StartDateString, DbType.String);
            sqlParameters.Add("@StartPushUpsCount", pushUps.StartPushUpsCount, DbType.Int32);
            sqlParameters.Add("@MaxPushUpsByDay", pushUps.MaxPushUpsByDay, DbType.Int32);
            sqlParameters.Add("@IncrementPushUpsByDay", pushUps.IncrementPushUpsByDay, DbType.Int32);
            //sqlParameters.Add("@UserId", user.City, DbType.String);

            string sql = @"EXEC KlikovnikSchema.spPushUpsInitialData_Insert
                @StartDayString NVARCHAR(255),
                @StartPushUpsCount INT,
                @MaxPushUpsByDay INT,
                @IncrementPushUpsByDay INT";
                //@UserId INT";

            return _dapper.ExecuteSqlWithParameters(sql, sqlParameters);
        }

        public bool InsertAuth(UserForLoginDTO userForSetPassword, byte[] passwordHash, byte[] passwordSalt)
        {
            string sqlAddAuth = @"EXEC KlikovnikSchema.spRegistration_Insert 
                    @Email = @Email , @PasswordHash = @PasswordHashParam, @PasswordSalt = @PasswordSaltParam";

            DynamicParameters sqlParameters = new();
            sqlParameters.Add("@Email", userForSetPassword.Email, DbType.String);
            sqlParameters.Add("@PasswordHashParam", passwordHash, DbType.Binary);
            sqlParameters.Add("@PasswordSaltParam", passwordSalt, DbType.Binary);
             
            return _dapper.ExecuteSqlWithParameters(sqlAddAuth, sqlParameters);
        }

        public bool CheckIfEmailAlreadyExist(string email)
        {
            string sql = "SELECT * FROM KlikovnikSchema.Auth WHERE @Email = @Email";

            DynamicParameters dynamicParameters = new();
            dynamicParameters.Add("@Email", email, DbType.String);

            return _dapper.LoadDataWithParameters<string>(sql, dynamicParameters).Count() == 0;
        }
    }
}