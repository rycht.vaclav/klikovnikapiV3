namespace KlikovnikAPIV3
{
    public static class Constants
    {
        //Dial codes
        public static string[] DialCodes {get;} = {"+420"};

        //Phone number format states
        public const string WrongNumberFormat = "Number is in wrong format!";
        public const string NonExistingDial = "This dial code doesn't exist!";
        public const string NumberOk = "Ok";

        //Email format states
        public const string WrongEmailFormat = "Email is in wrong format!";
        public const string WrongEmailDomainFormat = "Email domain doesn't exist!";
        public const string EmailOk = "Ok";
    }
}